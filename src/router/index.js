import Vue from 'vue';
import VueRouter from 'vue-router';

import Login from '../components/Login';
import Home from '../components/Home';

Vue.use(VueRouter)

const routes = [
    {
		path: '/login',
		component: Login
    },
    {
		path: '/',
		component: Home
	}
]

const router = new VueRouter({
    mode: 'history',
    routes,
})

export default router;